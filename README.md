By Andre Araujo, March 23, 2015

This was adapted from dotfiles.git, by the Startup Engineering class.
I have done some modifications (and might do more in the future), that's why I committed my own version.
This was adapted to work better for Mac.

To adequately configure the programming environment in your Mac, do the following:

```sh
cd $HOME
git clone https://andrefaraujo@bitbucket.org/andrefaraujo/dotfiles-mac.git
ln -s dotfiles-mac/.screenrc .
ln -s dotfiles-mac/.bash_profile .
ln -s dotfiles-mac/.bashrc .
ln -s dotfiles-mac/.bashrc_custom .
mv .emacs.d .emacs.d~
ln -s dotfiles-mac/.emacs.d .
```

In addition to this,

1) You probably want to configure a better look for your terminal:
Go to Terminal->Preferences, then under the tab "Settings", use the Pro default.
Under "Text", check all boxes, change the color of the cursor to saturated pink, check to blink, and change font size to 14.
Under "Window", under "Background", click on "Color & Effects" and choose
Opacity at 80% and Blur at 100%.
Under "Keyboard". check "Use option as meta key".

2) Also, make sure to have re-mapped CAPS LOCK to control key under the Mac's preferences.

3) Download the latest emacs for Mac from https://emacsformacosx.com/ . This will install the GUI version of emacs. The command "em" from the terminal will automatically call this program.

4) Emacs: it will download and install el-get in the first usage of emacs. To download "auto-complete", do: "<M-x> el-get-install lua-mode". To download  "lua-mode" for Lua interpreting on emacs, do: "<M-x> el-get-install lua-mode"

LICENSE for this code: MIT